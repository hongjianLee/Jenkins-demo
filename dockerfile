FROM openjdk:8

ADD ./target/demo-0.0.1-SNAPSHOT.jar /home/springboot/demo-0.0.1-SNAPSHOT.jar

EXPOSE 8081

ENTRYPOINT ["java","-jar","/home/springboot/demo-0.0.1-SNAPSHOT.jar"]